package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.SetmealDish;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.exception.SetmealEnableFailedException;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.SetmealService;
import com.sky.vo.DishItemVO;
import com.sky.vo.SetmealVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.NotAcceptableStatusException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @Author:cola
 **/
@Service
public class SetmealServiceImpl implements SetmealService {

    @Autowired
    private SetmealMapper setmealMapper;

    @Autowired
    private SetmealDishMapper setmealDishMapper;

    @Autowired
    private DishMapper dishMapper;

    @Override
    @Transactional
    public void save(SetmealDTO setmealDTO) {
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(setmealDTO, setmeal);

        //向套餐表中插入一条数据
        setmealMapper.insert(setmeal);

        Long id = setmeal.getId();

        //向套餐菜品表中插入n条数据
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        if (setmealDishes != null && setmealDishes.size() > 0) {
            for (SetmealDish item : setmealDishes) {
                item.setSetmealId(id);
            }
            setmealDishMapper.insert(setmealDishes);

        }
    }

    /**
     * 分页查询
     *
     * @param setmealPageQueryDTO
     * @return
     */
    @Override
    public PageResult pageQuery(SetmealPageQueryDTO setmealPageQueryDTO) {
        PageHelper.startPage(setmealPageQueryDTO.getPage(), setmealPageQueryDTO.getPageSize());

        Page<SetmealVO> page = setmealMapper.pageQuery(setmealPageQueryDTO);
        return new PageResult(page.getTotal(), page.getResult());

    }


    /**
     * 批量删除套餐
     *
     * @param ids
     */
    @Transactional
    @Override
    public void deleteByIds(List<Long> ids) {
        for (Long item : ids) {
            if (item.equals(StatusConstant.DISABLE)) {
                throw new DeletionNotAllowedException("套餐启用中,不能删除");
            }
        }
        //删套餐表
        setmealMapper.deleteByIds(ids);
        //删套餐菜品关系表
        setmealDishMapper.deleteBySetmealIds(ids);

    }

    /**
     * 修改套餐
     *
     * @param setmealDTO
     */
   // @Transactional
    @Override
    public void updateSetmeal(SetmealDTO setmealDTO) {
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(setmealDTO, setmeal);

        //修改套餐表中的数据
        setmealMapper.update(setmeal);

        Long setmealId = setmealDTO.getId();

        //修改套餐关系表中的数据
        List<Long> idList = new ArrayList<>();
        idList.add(setmealId);
        setmealDishMapper.deleteBySetmealIds(idList);

        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();

        if (setmealDishes != null && setmealDishes.size() > 0) {
            for (SetmealDish item : setmealDishes) {
                item.setSetmealId(setmealId);
            }
            setmealDishMapper.insert(setmealDishes);
        }
    }



    /**
     * 根据id查询套餐
     * @param id
     */
    @Override
    public SetmealDTO getById(Long id) {

        Setmeal setmeal = setmealMapper.getById(id);

        Long setmealId = setmeal.getId();


       List<SetmealDish> setmealDishes =  setmealDishMapper.getBySetmealId(setmealId);
        SetmealDTO setmealDTO = new SetmealDTO();
        BeanUtils.copyProperties(setmeal,setmealDTO);
        setmealDTO.setSetmealDishes(setmealDishes);

        return setmealDTO;
    }

    /**
     * 起售、停售套餐
     * @param status
     * @param id
     */
    @Override
    public void startOrStop(Integer status, Long id) {
        //起售时，如果套餐中含有停售的菜品，则不能起售

        if (status==StatusConstant.ENABLE){
           List<Dish> dishList = dishMapper.getBySetmealId(id);
           for (Dish dish : dishList){
                if (dish.getStatus()==StatusConstant.DISABLE){
                    throw new SetmealEnableFailedException(MessageConstant.SETMEAL_ENABLE_FAILED);
                }
           }
        }
        Setmeal setmeal = new Setmeal();
        setmeal.setId(id);
        setmeal.setStatus(status);
        setmealMapper.update(setmeal);

    }

    /**
     * 条件查询
     * @param setmeal
     * @return
     */
    public List<Setmeal> list(Setmeal setmeal) {
        List<Setmeal> list = setmealMapper.list(setmeal);
        return list;
    }


    /**
     * 根据id查询菜品选项
     * @param id
     * @return
     */
    public List<DishItemVO> getDishItemById(Long id) {
        return setmealMapper.getDishItemBySetmealId(id);
    }

}









