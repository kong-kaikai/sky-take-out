package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.annotation.AutoFill;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author:cola
 **/
@Mapper
public interface CategoryMapper {

    /**
     * 分页查询分类
     *
     * @param categoryPageQueryDTO
     * @return
     */
     Page<Category> selectByPage(CategoryPageQueryDTO categoryPageQueryDTO) ;


    /**
     * 新增分类
     * @param category
     */
    @Insert("insert into category" +
            "( type, name, sort, status, create_time, update_time, create_user, update_user) " +
            "values " +
            "(#{type},#{name},#{sort},#{status},#{createTime},#{updateTime},#{createUser},#{updateUser})")
    @AutoFill(value = OperationType.INSERT)
    void insert(Category category);

    /**
     * 启用、禁用分类
     * 修改分类
     * @param category
     */
    @AutoFill(value = OperationType.UPDATE)
    void update(Category category);

    @Delete("delete from category where id=#{id}")
    void deleteById(Long id);


    /**
     * 根据类型查询分类
     * @param type
     * @return
     */
    List<Category> list(Integer type);
}
