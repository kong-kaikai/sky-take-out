package com.sky.mapper;

import com.sky.annotation.AutoFill;
import com.sky.entity.SetmealDish;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Author:cola
 **/

@Mapper
public interface SetmealDishMapper {



    /**
     * 根据菜品id查询对应的套餐id
     * @param dishIds
     * @return
     */
    //select setmeal_id fron setmeal_dish where dish_id in ()
    List<Long> getSetmealIdsByDishIds(List<Long> dishIds);



    /**
     * 新增套餐
     * @param setmealDishes
     */

    void insert(List<SetmealDish> setmealDishes);


    /**
     * 根据setmel_id删除数据
     * @param ids
     */
    void deleteBySetmealIds(List<Long> ids);


    /**
     * 根据套餐id查询套餐和菜品的关联关系
     * @param setmealId
     * @return
     */
    @Select("select * from setmeal_dish where setmeal_id = #{setmealId}")
    List<SetmealDish> getBySetmealId(Long setmealId);


}
